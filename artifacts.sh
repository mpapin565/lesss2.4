PIPELINE_ID=$(curl --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/37498546/pipelines" | jq '.[0] | .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/37498546/pipelines/$PIPELINE_ID/jobs" | jq ' .[] | select(.stage == "build_dev") | .id')
curl --output build_dev.txt --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/37498546/jobs/$JOB_ID/artifacts/$ARTIFACT"

