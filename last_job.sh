 #!/bin/bash
LAST_JOB_ID=$(curl --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/37498546/jobs" | jq -r '[.[] | select((.stage=="build_dev") and (.pipeline.status=="success"))][0] | .id') 
ARTIFACT=$(curl --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/37498546/jobs/$LAST_JOB_ID/artifacts/build_dev.txt" --output ./build_dev.txt)

